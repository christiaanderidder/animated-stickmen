var express = require('../app/lib/node_modules/express');
var io = require('../app/lib/node_modules/socket.io'); 

var app = express();
var server = app.listen(1223, function() {
    console.log('Listening on port %d', server.address().port);
});
console.log(__dirname);
app.get('/maps/:id', function(request, response){
    var mapId = request.params.id;
    
});
app.use(express.static(__dirname + '/../app'));
app.use(express.static(__dirname + '/../client'));


var socket = io.listen(server);
var players = [];

socket.on("connection", function (client) {  

	// Receive event
    client.on("player:join", function(nickname) {
		
        var player = {
            name: nickname
        }

        players[client.id] = player;

		// Add player to list of players
        console.log(player.name + ' joined');
	
    	// To the player
        client.emit("server:message", { message: 'welcome ' + player.name });

        // To everybody except the player
        client.broadcast.emit("server:message", { message: player.name + ' joined' });

        // To everybody
        socket.sockets.emit("server:message", { message: 'message to everybody' });
    });

    // Receive disconnect event
    client.on("disconnect", function() {

		// Remove player from the list of players
        var player = players[client.id];
        if(player != null){
            client.broadcast.emit("server:message", {message: player.name + ' left'});
        }
        delete players[client.id];
		
    });

});
