// Configure Require.js
var require = {
	// Default load path for js files
	baseUrl: '/code/',
	shim: {
		ThreeCore: { exports: 'THREE' },
		ThreeXKeyboardState: { exports: 'THREEx' }
	},
	paths: {
		ThreeCore: '../lib/bower_components/threejs/build/three.min',
		ThreeXKeyboardState: '../lib/bower_components/threex.keyboardstate/threex.keyboardstate',
	}
};