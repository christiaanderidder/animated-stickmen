define(["ThreeCore", "ThreeXKeyboardState"], function(_three, _threex) {

	Player = function() {
		var texture = _three.ImageUtils.loadTexture("/assets/sprites/player.png");
		var geometry = new _three.PlaneGeometry(0.25, 0.25);
		var material = new _three.MeshBasicMaterial( { map: texture, wireframe: false });

		this._keyboard = new _threex.KeyboardState();
		// Call "base"
		_three.Mesh.call(this, geometry, material);		
	};

	Player.prototype = Object.create(_three.Mesh.prototype); // Make Player have the same methods as Mesh "inheritance"
	Player.prototype.constructor = Player; // Make sure the right constructor gets called

	Player.prototype.update = function(delta) {

		if(this._keyboard.pressed("up")) {
			this.position.y += 0.05;
		} else if(this._keyboard.pressed("down")) {
			this.position.y -= 0.05;
		}

		if(this._keyboard.pressed("right")) {
			this.position.x += 0.05;
		} else if(this._keyboard.pressed("left")) {
			this.position.x -= 0.05;
		}
	};

	return Player;
});