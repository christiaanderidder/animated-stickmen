define( [], function () {
	function Container(){
		this._element = document.getElementById("wrapper");
	}

	Container.prototype.get = function(){
		return this._element;
	}
	
	var instance = new Container();
	return instance.get();
});