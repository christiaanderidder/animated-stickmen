define( ["ThreeCore", "Camera"], function( _three, _camera ) {

	function Scene() {
		this._camera = _camera;

		this._scene = new _three.Scene();
		this._scene.add(this._camera);	

		this._addLight();
	}

	Scene.prototype._addLight = function() {
		var pointLight = new _three.PointLight(0xFFFFFF);
		pointLight.position.x = 10;
		pointLight.position.y = 50;
		pointLight.position.z = 130;

		this._scene.add(pointLight);
	}

	Scene.prototype.get = function(){
		return this._scene;
	}

	// "Singleton"
	var instance = new Scene();
	return instance.get();
});