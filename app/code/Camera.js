define( ["ThreeCore", "Container", "Scene"], function( _three, _container, _scene ) {

	function Camera() {
		this._camera = new _three.PerspectiveCamera(70, 1, 0.1, 5000);
		this._camera.position.z = 3;
		this._element = _container;
		this._scene = _scene;

		var self = this;
		window.addEventListener("resize", function(){
			self._onResize();
		}, false);
		this._onResize();
	}

	Camera.prototype._onResize = function () {
		this._camera.aspect = this._element.offsetWidth / this._element.offsetHeight;
		this._camera.updateProjectionMatrix();
	};

	Camera.prototype.get = function(){
		return this._camera;
	}

	// "Singleton"
	var instance = new Camera();
	return instance.get();
});