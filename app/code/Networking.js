define(function() {

	function Networking() {
		
	}

	Networking.prototype.connect = function(host, nickname) {
		var self = this;

		this._connection = io.connect(self._host);

	    this._connection.emit("player:join", nickname);
	    this._connection.on("server:message", function(data){self._onMessage(data)});
	    this._connection.on("disconnect", function(){self._onDisconnect()});
	}

	Networking.prototype._onMessage = function(data){
		console.log('Message: ' + data.message);
	}

	Networking.prototype._onDisconnect = function(){
		console.log('Error: connection lost');
	}

	var instance = new Networking();
	return instance;

});
