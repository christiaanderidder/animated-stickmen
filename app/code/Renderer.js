define( ["ThreeCore", "Container"], function( _three, _container ) {

	function Renderer() {
		this._element = _container;
		this._element.innerHTML = "";
		this._renderer = new THREE.WebGLRenderer();
		//this._renderer.setSize( 800, 600 );
		this._renderer.setClearColor( 0xFF0000, 1);

		//this._renderer.sortObjects = false;
		this._renderer.autoClear = false;
		this._element.appendChild( this._renderer.domElement );

		var self = this;
		window.addEventListener("resize", function(){
			self._onResize();
		}, false);
		this._onResize();
	}

	Renderer.prototype._onResize = function() {
		console.log(this._element.offsetWidth);
		this._renderer.setSize( this._element.offsetWidth, this._element.offsetHeight );
	};


	Renderer.prototype.get = function(){
		return this._renderer;
	}

	// "Singleton"
	var instance = new Renderer();
	return instance.get();
} );