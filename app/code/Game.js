define(["ThreeCore", "Renderer", "Camera", "Scene", "Networking", "Player"],
function( _three, _renderer, _camera, _scene, _networking, Player) {

	function Game() {

		this._isPaused = false;

		this._renderer = _renderer;
		this._camera = _camera;
		this._netorking = _networking;
		this._scene = _scene;

		var material = new _three.MeshLambertMaterial({ color: 0x00FF00 });
		this._cube = new _three.Mesh(new _three.BoxGeometry(1,1,1), material);
		this._cube.position.z = -2;

		this._player = new Player();
		this._scene.add(this._player);
		this._scene.add(this._cube);


		this._previousTime = Date.now();
	}

	Game.prototype._update = function() {
		//update

		var currentTime = Date.now();
		var delta = currentTime - this._previousTime;
		this._previousTime = currentTime;

		this._cube.rotation.x += 0.01;
		this._cube.rotation.y += 0.01;

		this._player.update(delta);
	}

	Game.prototype._draw = function() {
		//draw

		// Autoclear is off (http://stackoverflow.com/questions/11110652/three-js-render-a-given-object-first)
		this._renderer.clear();
		this._renderer.render(this._scene, this._camera);
	}

	Game.prototype._loop = function() {
		var self = this;

		if(!this._isPaused){
			this._update();
			this._draw();
		}
	
		requestAnimationFrame(function(){
			self._loop()
		});
	}

	Game.prototype.run = function(){
		this._netorking.connect('127.0.0.1:1223', 'Christiaan');
		this._loop();
	}

	Game.prototype.pause = function(){
		this._isPaused = true;
	}

	Game.prototype.resume = function(){
		this._isPaused = false;
	}

	var game = new Game();
	return game;

});